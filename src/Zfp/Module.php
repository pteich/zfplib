<?php

namespace Zfp;

use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mvc\MvcEvent;

class Module implements ServiceProviderInterface
{
    public function onBootstrap($e)
    {
        $application = $e->getApplication();
        /** @var $serviceManager \Zend\ServiceManager\ServiceManager */
        $serviceManager = $application->getServiceManager();

        $this->setPhpSettings($serviceManager);
        $this->initDoctrine($serviceManager);
    }

    protected function initDoctrine(ServiceLocatorInterface $serviceManager)
    {
        /** @var $entityManager \Doctrine\ORM\EntityManager */
        if ($serviceManager->has('doctrine.entitymanager.orm_default')) {
            $entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');

            $serviceManager->setAlias('EntityManager', 'doctrine.entitymanager.orm_default');
            /*
            $entityManager->getEventManager()->addEventListener(
                array(\Doctrine\ORM\Events::postLoad),
                new PostLoadSubscriber($serviceManager)
            );
            */
        }
    }

    protected function setPhpSettings($serviceManager)
    {
        $config      = $serviceManager->get('Configuration');
        $phpSettings = $config['phpSettings'];
        if($phpSettings) {
            foreach($phpSettings as $key => $value) {
                ini_set($key, $value);
            }
        }
    }

    public function getConfig()
    {
        return array(
            'phpSettings' => array(
                'date.timezone' => 'Europe/Berlin',
            ),
            'view_helpers' => array(
                'invokables' => array(
                ),
                'aliases' => array(
                )
            ),
        );
    }

    public function getAutoloaderConfig()
    {
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'shorttext' => function ($pluginManager) {
                        return new View\Helper\Shorttext();
                },
                'urlslug' => function ($pluginManager) {
                        return new View\Helper\Slug();
                }
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'DomainServiceLoader' => 'Zfp\Service\DomainServiceLoaderFactory',
                'Zfp\Service\Facebook'  => function($sm) {
                        $config = $sm->get('Configuration');
                        $fb = new \Zfp\Service\Facebook($config['facebook']);
                        return $fb;
                    },

                'Zfp\Service\Google'  => function($sm) {
                        $config = $sm->get('Configuration');
                        $google = new \Zfp\Service\Google($config['google']);
                        return $google;
                    },

                'Zfp\Service\Flickr'  => function($sm) {
                    $config = $sm->get('Configuration');
                    $flickr = new \Zfp\Service\Flickr($config['flickr']);
                    return $flickr;
                },

                'Zfp\Service\Instagram'  => function($sm) {
                    $config = $sm->get('Configuration');
                    $instagram = new \Zfp\Service\Instagram($config['instagram']);
                    return $instagram;
                },

                'Zfp\Service\Foursquare'  => function($sm) {
                    $config = $sm->get('Configuration');
                    $foursquare = new \Zfp\Service\Foursquare($config['foursquare']);
                    return $foursquare;
                },

                'Zfp\Service\Twitter'  => function($sm) {
                    $config = $sm->get('Configuration');
                    $twitter = new \Zfp\Service\Twitter($config['twitter']);
                    return $twitter;
                },

                'Zfp\Service\Geoip'  => function($sm) {
                        return new \Zfp\Service\Geoip($sm->get('zfp.geoip.cache'));
                    },
            ),
            'invokables' => array(
                'Zfp\Service\Geocoder' => 'Zfp\Service\Geocoder',
                //'Zfp\Service\Geoip' => 'Zfp\Service\Geoip',
                'EmptyArrayFilter' => 'Zfp\Filter\EmptyArray'
            ),
            'aliases' => array(
                'ZfpGeocoder' => 'Zfp\Service\Geocoder',
                'ZfpGeoip' => 'Zfp\Service\Geoip'
            )
        );
    }
}
