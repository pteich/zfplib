<?php

namespace Zfp\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Slug extends AbstractHelper
{

    public function __invoke($string)
    {
        $search = array("'");
        $replace = array("");

        //$string = strtolower(trim($string));
        $string = str_replace($search, $replace, $string);

        /*
        $string = preg_replace(array("#[,./]#","#[^a-zA-Z0-9]#"),array("","-"),$string);

        $string = preg_replace('/\-+/','-',$string);
        */
        $string = preg_replace('/\W+/','_',$string);
        return $string;
    }
}
