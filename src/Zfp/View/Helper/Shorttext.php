<?php

namespace Zfp\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Shorttext extends AbstractHelper
{

    public function __invoke($text, $length, $strip_tags = false)
    {
        if ($strip_tags) {
            $text = strip_tags($text);
        }
        $shorttext = $text;
        if (strlen($text) > $length) {
            $shorttext = substr($text, 0, $length);
            $var = explode(" ", substr($text, $length));
            if (strlen($var[0]) < 30) {
                $shorttext .= $var[0];
            }
            $shorttext .= " ...";
        }
        return $shorttext;
    }
}
