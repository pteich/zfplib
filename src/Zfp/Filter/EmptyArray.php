<?php

namespace Zfp\Filter;

use Zend\Filter;

class EmptyArray implements \Zend\Filter\FilterInterface {

    public function filter($value)
    {
        if(!$value || empty($value)) {
            $value = '[]';
        }
        return $value;
    }

}