<?php

namespace Zfp\Service;

use Guzzle\Http\Client;

class Instagram
{

    /**
     * @var \Guzzle\Http\Client
     */
    protected $httpClient = null;

    protected $client_id = null;
    protected $client_secret = null;
    protected $access_token = null;

    public function __construct($config)
    {
        $this->httpClient = new Client('https://api.instagram.com');
        $this->client_id = $config['client_id'];
        $this->client_secret = $config['client_secret'];
        $this->access_token = $config['access_token'];
    }

    public function getAccessToken($code, $redirect_uri)
    {
        $request = $this->httpClient->post('/oauth/access_token', null, array(
            'client_id'     => $this->client_id,
            'client_secret' => $this->client_secret,
            'grant_type'    => 'authorization_code',
            'redirect_uri'  => $redirect_uri,
            'code'          => $code
        ));

        $response = $request->send();

        $data = $response->json();

        return $data['access_token'];
    }

    public function searchPeople($name)
    {
        $request = $this->httpClient->createRequest('GET','/v1/users/search');
        $request->getQuery()
            ->set('q',$name)
            ->set('access_token',$this->access_token);

        $response = $request->send();
        $data = $response->json();

        if ($data['meta']['code']==200) {
            return $data['data'];
        } else {
            return arry();
        }
    }

    public function getUserDetails($id)
    {
        $request = $this->httpClient->createRequest('GET','/v1/users/'.$id);
        $request->getQuery()
            ->set('access_token',$this->access_token);

        $response = $request->send();
        $data = $response->json();

        if ($data['meta']['code']==200) {
            return $data['data'];
        } else {
            return arry();
        }
    }

}