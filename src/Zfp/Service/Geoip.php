<?php

namespace Zfp\Service;

use Zend\Http\Client;

class Geoip
{

    /**
     * @var string
     */
    const ENDPOINT_URL = 'http://freegeoip.net/json/%s';

    /**
     * @var \Zend\Http\Client
     */
    protected $client = null;

    /**
     * @var \Zend\Cache\Storage\Adapter\AbstractAdapter
     */
    protected $cache = null;


    public function __construct($cache)
    {
        $this->setClient(new Client());
        $this->setCache($cache);
    }

    /**
     * @param \Zend\Cache\Storage\Adapter\AbstractAdapter $cache
     */
    public function setCache($cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return \Zend\Cache\Storage\Adapter\AbstractAdapter
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * @param \Zend\Http\Client $adapter
     */
    public function setClient($adapter)
    {
        $this->client = $adapter;
    }

    /**
     * @return \Zend\Http\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    public function getGeocodedData($address)
    {
        if (!filter_var($address, FILTER_VALIDATE_IP)) {
            throw new UnsupportedException('Only IP addresses allowed.');
        }

        if (in_array($address, array('127.0.0.1', '::1'))) {
            return array($this->getLocalhostDefaults());
        }

        $cachekey = md5('GeoIP'.$address);

        $result = $this->getCache()->getItem($cachekey);

        if (!$result) {
            $query = sprintf(self::ENDPOINT_URL, $address);
            $result = $this->executeQuery($query);
            $this->getCache()->setItem($cachekey,$result);
        }

        return $result;
    }

    protected function executeQuery($query)
    {
        try {
            $response = $this->client->setUri($query)->send();
            $content = $response->isSuccess() ? $response->getBody() : null;
        } catch (\Exception $e) {
            $content = null;
        }

        if (null === $content) {
            //throw new Exception(sprintf('Could not execute query %s', $query));
            return array($this->getLocalhostDefaults());
        }

        if (preg_match('/not found/i',$content)) {
            return array($this->getLocalhostDefaults());
        }

        $data = (array)json_decode($content);

        if (empty($data)) {
            throw new NoResultException(sprintf('Could not execute query %s', $query));
        }

        return array(array_merge($this->getDefaults(), array(
            'latitude'    => isset($data['latitude']) ? $data['latitude'] : null,
            'longitude'   => isset($data['longitude']) ? $data['longitude'] : null,
            'city'        => isset($data['city']) ? $data['city'] : null,
            'zipcode'     => isset($data['zipcode']) ? $data['zipcode'] : null,
            'region'      => isset($data['region_name']) ? $data['region_name'] : null,
            'regionCode'  => isset($data['region_code']) ? $data['region_code'] : null,
            'country'     => isset($data['country_name']) ? $data['country_name'] : null,
            'countryCode' => isset($data['country_code']) ? $data['country_code'] : null,
        )));
    }

    protected function getDefaults()
    {
        return array(
            'latitude'     => null,
            'longitude'    => null,
            'bounds'       => null,
            'streetNumber' => null,
            'streetName'   => null,
            'city'         => null,
            'zipcode'      => null,
            'cityDistrict' => null,
            'county'       => null,
            'countyCode'   => null,
            'region'       => null,
            'regionCode'   => null,
            'country'      => null,
            'countryCode'  => null,
            'timezone'     => null,
        );
    }

    protected function getLocalhostDefaults()
    {
        return array(
            'latitude'  => 51.3,
            'longitude' => 12.3333,
            'city'      => 'localhost',
            'region'    => 'localhost',
            'county'    => 'localhost',
            'country'   => 'localhost',
        );
    }
}
