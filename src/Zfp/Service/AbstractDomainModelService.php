<?php

namespace Zfp\Service;

use \Zend\EventManager\EventManagerInterface;
use \Zend\EventManager\EventManager;


abstract class AbstractDomainModelService
    extends AbstractDomainService
    implements \Zend\EventManager\EventManagerAwareInterface
{

    const GET_ARRAY = \Doctrine\ORM\Query::HYDRATE_ARRAY;
    const GET_OBJECT = \Doctrine\ORM\Query::HYDRATE_OBJECT;

    /**
     * The full namespaced class name of the Domain Model
     * that this DomainService instance Represents
     *
     * @var string
     */
    protected $entity = '';

    /**
     * Inject an EventManager instance
     *
     * @param  EventManagerInterface $eventManager
     * @return AbstractDomainModelService
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $eventManager->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $eventManager;
        return $this;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->events) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }

    /**
     * @param bool $sortBy
     * @param int $getArray
     * @return array
     */
    public function getAllActive($sortBy=false,$getArray=self::GET_OBJECT)
    {
        $em = $this->getEntityManager();
        $queryBuilder = $em->createQueryBuilder()
            ->select('e')
            ->from($this->entity, 'e');

        $queryBuilder->where('e.visible=:visible')->setParameter('visible',1);

        if ($sortBy) {
            $queryBuilder->orderBy($sortBy);
        } elseif ($this->hasField('sort')) {
            $queryBuilder->orderBy('e.sort');
        }

        $query = $queryBuilder->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->useQueryCache(false);
        $query->useResultCache(false);

        return $query->getResult($getArray);
    }

    /**
     * Returns all models in the collection
     *
     * @return array
     */
    public function getAll()
    {
        $em = $this->getEntityManager();
        $queryBuilder = $em->createQueryBuilder()
            ->select('e')
            ->from($this->entity, 'e');

        if ($this->hasField('sort')) {
            $queryBuilder->orderBy('e.sort');
        }

        $query = $queryBuilder->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');

        return $query->getResult();
    }

    protected function hasField($field)
    {
        return $this->getEntityManager()->getClassMetadata($this->entity)->hasField($field);
    }

    /**
     * Returns the model instance identified by the passed value
     * @param $id
     * @param bool $cache
     * @param int $ttl
     * @return mixed
     * @throws Exception
     */
    public function getById($id, $cache=false, $ttl=60)
    {

        $query = $this->getEntityManager()
            ->createQuery('SELECT e FROM '.$this->entity.' e WHERE e.id = :id')
            ->setParameter('id', $id)
            ->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');

        //$query->useResultCache(false,$ttl,get_class($this).'_'.$id);

        $entities = $query->getResult();

        if (!count($entities)) {
            throw new \Zfp\Service\Exception($this->entity.' not found with id: '.$id);
        }
        return $entities[0];
    }

    public function save($entity)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    public function getEntityAsImpagerString()
    {
        return preg_replace('/\\\/','_',$this->entity);
    }

}
