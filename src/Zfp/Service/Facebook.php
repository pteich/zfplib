<?php

namespace Zfp\Service;


class Facebook {

    /**
     * @var \Zend\Http\Client
     */
    protected $client = null;

    /**
     * @var \Facebook
     */
    protected $fb = null;

    public function __construct($config)
    {
        $this->fb = new \Facebook($config);
    }

    public function getFeed($name,$limit=false)
    {
        $options = array('summary'=>1,'locale'=>'de_DE');
        if ($limit) {
            $options['limit'] = $limit;
        }
        try {
            $data = $this->fb->api("/{$name}/feed", 'GET', $options);
        } catch(FacebookApiException $e) {
            $data['data'] = array();
        }

        return $data['data'];
    }

    public function searchPage($name)
    {
        try {
            $data = $this->fb->api("/search", 'GET', array('type'=>'page', 'q'=>$name));
        } catch(FacebookApiException $e) {
            $data['data'] = array();
        }

        return $data['data'];
    }

    public function getDetails($id)
    {
        try {
            $data = $this->fb->api("/{$id}", 'GET');
        } catch(FacebookApiException $e) {
            $data = array();
        }

        return $data;
    }

    public function getImage($id)
    {
        return "https://graph.facebook.com/{$id}/picture?type=large";
    }

}
