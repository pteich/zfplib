<?php

namespace Zfp\Service;

use Guzzle\Http\Client;

class Twitter
{

    /**
     * @var \Guzzle\Http\Client
     */
    protected $httpClient = null;

    protected $client_id = null;
    protected $client_secret = null;
    protected $access_token = null;

    public function __construct($config)
    {
        $this->httpClient = new Client('https://api.twitter.com/{version}', array(
            'version' => '1.1'
        ));

        $this->httpClient->addSubscriber(new \Guzzle\Plugin\Oauth\OauthPlugin(array(
            'consumer_key'  => $config['consumer_key'],
            'consumer_secret' => $config['consumer_secret'],
            'token'       => $config['access_token'],
            'token_secret'  => $config['access_token_secret']
        )));
    }

    public function searchPeople($name)
    {
        $request = $this->httpClient->get('users/search.json');
        $request->getQuery()->set('q',$name);

        $response = $request->send();
        $data = $response->json();

        if (is_array($data)) {
            return $data;
        } else {
            return arry();
        }
    }

    public function getUserDetails($id)
    {
        $request = $this->httpClient->get('users/show.json');
        if (is_int($id)) {
            $request->getQuery()->set('user_id',$id);
        } else {
            $request->getQuery()->set('screen_name', $id);
        }

        $response = $request->send();
        $data = $response->json();

        return $data;

    }

}