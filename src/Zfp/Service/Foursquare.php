<?php

namespace Zfp\Service;

use Guzzle\Http\Client;

class Foursquare
{

    /**
     * @var \Guzzle\Http\Client
     */
    protected $httpClient = null;

    protected $client_id = null;
    protected $client_secret = null;
    protected $access_token = null;

    public function __construct($config)
    {
        $this->httpClient = new Client('https://api.foursquare.com');
        $this->client_id = $config['client_id'];
        $this->client_secret = $config['client_secret'];
        $this->access_token = $config['access_token'];
    }

    public function getAccessToken($code, $redirect_uri)
    {
        $this->httpClient->setBaseUrl('https://foursquare.com');
        $request = $this->httpClient->post('/oauth2/access_token', null, array(
            'client_id'     => $this->client_id,
            'client_secret' => $this->client_secret,
            'grant_type'    => 'authorization_code',
            'redirect_uri'  => $redirect_uri,
            'code'          => $code
        ));

        $response = $request->send();

        $data = $response->json();

        return $data['access_token'];
    }

    public function searchPeople($name)
    {
        $request = $this->httpClient->createRequest('GET','/v2/users/search');
        $request->getQuery()
            ->set('name',$name)
            ->set('v','20140527')
            ->set('oauth_token',$this->access_token);

        $response = $request->send();
        $data = $response->json();

        if ($data['meta']['code']==200) {
            return $data['response']['results'];
        } else {
            return arry();
        }
    }

    public function getUserDetails($id)
    {
        $request = $this->httpClient->createRequest('GET','/v2/users/'.$id);
        $request->getQuery()
            ->set('v','20140527')
            ->set('oauth_token',$this->access_token);

        $response = $request->send();
        $data = $response->json();

        if ($data['meta']['code']==200) {
            return $data['response']['user'];
        } else {
            return arry();
        }
    }

}