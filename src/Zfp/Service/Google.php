<?php

namespace Zfp\Service;


class Google {

    /**
     * @var \Google_Client
     */
    protected $google = null;

    public function __construct($config)
    {
        $this->google = new \Google_Client();
        $this->google->setApplicationName($config['application_name']);
        $this->google->setDeveloperKey($config['developer_key']);
    }

    /**
     * @param $name
     * @return \Google_Service_Plus_PeopleFeed
     */
    public function searchPeople($name,$limit=50)
    {
        $service = new \Google_Service_Plus($this->google);

        $data = $service->people->search($name,array('language'=>'de'));

        return $data;
    }

    /**
     * @param $peopleId
     * @return \Google_Service_Plus_Person
     */
    public function getUserDetails($peopleId)
    {
        $service = new \Google_Service_Plus($this->google);

        $data = $service->people->get($peopleId);

        return $data;
    }

    public function searchYoutube($q,$limit=50)
    {
        $service = new \Google_Service_YouTube($this->google);

        $data = $service->search->listSearch('id,snippet', array(
            'q' => $q,
            'maxResults' => $limit,
            'safeSearch' => 'none'
        ));

        return $data;
    }

}
