<?php

namespace Zfp\Service;

use Zend\Http\Client;

class Geocoder {

    /**
     * @var string
     */
    const ENDPOINT_URL = 'http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false';

    /**
     * @var string
     */
    const ENDPOINT_URL_SSL = 'https://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false';

    /**
     * @var \Zend\Http\Client
     */
    private $client = null;

    private $useSsl = false;

    private $region = null;

    private $locale = null;

    public function __construct()
    {
        // TODO Auf Factory umstellen, HTTPAdapter injizieren, Parameter übergabe
        $this->useSsl = false;
        $this->client = new Client();

        $this->region = 'de';
        $this->locale = 'DE_de';
    }

    public function getGeocodedData($address)
    {
        $query = sprintf(
            $this->useSsl ? self::ENDPOINT_URL_SSL : self::ENDPOINT_URL,
            rawurlencode($address)
        );

        return $this->executeQuery($query);
    }

    /**
     * @param string $query
     *
     * @return string Query with extra params
     */
    protected function buildQuery($query)
    {
        if (null !== $this->getLocale()) {
            $query = sprintf('%s&language=%s', $query, $this->getLocale());
        }

        if (null !== $this->getRegion()) {
            $query = sprintf('%s&region=%s', $query, $this->getRegion());
        }

        return $query;
    }

    protected function executeQuery($query)
    {
        $query = $this->buildQuery($query);

        try {
            $response = $this->client->setUri($query)->send();
            $content  = $response->isSuccess() ? $response->getBody() : null;
        } catch (\Exception $e) {
            $content = null;
        }

        if (null === $content) {
            throw new Exception(sprintf('Could not execute query %s', $query));
        }

        $json = json_decode($content);

        // API error
        if (!isset($json)) {
            throw new Exception(sprintf('Could not execute query %s', $query));
        }

        // you are over your quota
        if ('OVER_QUERY_LIMIT' === $json->status) {
            throw new Exception(sprintf('Daily quota exceeded %s', $query));
        }

        // no result
        if (!isset($json->results) || !count($json->results) || 'OK' !== $json->status) {
            throw new Exception(sprintf('Could not execute query %s', $query));
        }

        $results = array();

        foreach ($json->results as $result) {
            $resultset = $this->getDefaults();

            // update address components
            foreach ($result->address_components as $component) {
                foreach ($component->types as $type) {
                    $this->updateAddressComponent($resultset, $type, $component);
                }
            }

            // update coordinates
            $coordinates = $result->geometry->location;
            $resultset['latitude']  = $coordinates->lat;
            $resultset['longitude'] = $coordinates->lng;

            $resultset['bounds'] = null;
            if (isset($result->geometry->bounds)) {
                $resultset['bounds'] = array(
                    'south' => $result->geometry->bounds->southwest->lat,
                    'west'  => $result->geometry->bounds->southwest->lng,
                    'north' => $result->geometry->bounds->northeast->lat,
                    'east'  => $result->geometry->bounds->northeast->lng
                );
            } elseif ('ROOFTOP' === $result->geometry->location_type) {
                // Fake bounds
                $resultset['bounds'] = array(
                    'south' => $coordinates->lat,
                    'west'  => $coordinates->lng,
                    'north' => $coordinates->lat,
                    'east'  => $coordinates->lng
                );
            }

            $results[] = array_merge($this->getDefaults(), $resultset);
        }

        return $results;
    }


    /**
     * @param null|string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return null|string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param null|string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return null|string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param \Zend\Http\Client $adapter
     */
    public function setClient($adapter)
    {
        $this->client = $adapter;
    }

    /**
     * @return \Zend\Http\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Returns the default results.
     *
     * @return array
     */
    protected function getDefaults()
    {
        return array(
            'latitude'      => null,
            'longitude'     => null,
            'bounds'        => null,
            'streetNumber'  => null,
            'streetName'    => null,
            'city'          => null,
            'zipcode'       => null,
            'cityDistrict'  => null,
            'county'        => null,
            'countyCode'    => null,
            'region'        => null,
            'regionCode'    => null,
            'country'       => null,
            'countryCode'   => null,
            'timezone'      => null,
        );
    }

    /**
     * Update current resultset with given key/value.
     *
     * @param array  $resultset Resultset to update.
     * @param string $type      Component type.
     * @param object $values    The component values;
     *
     * @return array
     */
    protected function updateAddressComponent(&$resultset, $type, $values)
    {
        switch ($type) {
            case 'postal_code':
                $resultset['zipcode'] = $values->long_name;
                break;

            case 'locality':
                $resultset['city'] = $values->long_name;
                break;

            case 'administrative_area_level_2':
                $resultset['county'] = $values->long_name;
                $resultset['countyCode'] = $values->short_name;
                break;

            case 'administrative_area_level_1':
                $resultset['region'] = $values->long_name;
                $resultset['regionCode'] = $values->short_name;
                break;

            case 'country':
                $resultset['country'] = $values->long_name;
                $resultset['countryCode'] = $values->short_name;
                break;

            case 'street_number':
                $resultset['streetNumber'] = $values->long_name;
                break;

            case 'route':
                $resultset['streetName'] = $values->long_name;
                break;

            case 'sublocality':
                $resultset['cityDistrict'] = $values->long_name;
                break;

            default:
        }

        return $resultset;
    }

    /**
     * @param array $results
     *
     * @return array
     */
    protected function fixEncoding(array $results)
    {
        return array_map(function($value) {
            return is_string($value) ? utf8_encode($value) : $value;
        }, $results);
    }

}