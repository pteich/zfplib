<?php

namespace Zfp\Service;

use Guzzle\Http\Client;

class Flickr {

    /**
     * @var \Guzzle\Http\Client
     */
    protected $httpClient = null;

    protected $api_key = null;
    protected $secret = null;

    public function __construct($config)
    {
        $this->httpClient = new Client('https://api.flickr.com');

        $this->api_key = $config['api_key'];
        $this->secret = $config['secret'];
    }

    public function searchPhotos($name,$limit=50)
    {
        $request = $this->httpClient->post('/services/rest/', null, array(
            'api_key'       => $this->api_key,
            'method'        => 'flickr.photos.search',
            'format'        => 'php_serial',
            'text'          => $name,
            'per_page'      => $limit
        ));

        $response = $request->send();
        $data = unserialize($response->getBody(true));

        return $data['photos']['photo'];
    }

    public function getUserDetails($user_id)
    {
        $request = $this->httpClient->post('/services/rest/', null, array(
            'api_key'       => $this->api_key,
            'method'        => 'flickr.people.getInfo',
            'format'        => 'php_serial',
            'user_id'       => $user_id,
        ));

        $response = $request->send();
        $data = unserialize($response->getBody(true));
        return $data['person'];
    }

    public function getPhotourl($photo,$size='-')
    {
        return "https://farm{$photo['farm']}.staticflickr.com/{$photo['server']}/{$photo['id']}_{$photo['secret']}.jpg";
    }

    public function getUserimageurl($user)
    {
        if ($user['iconserver']>0) {
            return "http://farm{$user['iconfarm']}.staticflickr.com/{$user['iconserver']}/buddyicons/{$user['nsid']}.jpg";
        } else {
            return "https://www.flickr.com/images/buddyicon.gif";
        }
    }

}
