<?php

namespace Zfp\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 *
 * @Annotation
 * @Target("PROPERTY")
 *
 */
final class Geographical extends Annotation
{
    /** @var array<string> @required */
    public $fields = array();
    /** @var boolean */
    public $updatable = true;
    /** @var string */
    public $style = 'default'; // or "camel"
    /** @var string */
    public $unique_base = null;
    /** @var string */
    public $separator = '-';
    /** @var array<Gedmo\Mapping\Annotation\SlugHandler> */
    public $handlers = array();
}

